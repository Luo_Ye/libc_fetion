#include <stdio.h>

#include "fetion_libc.h"

int main(int argv, char *argc[])
{
	int ret;
	
	//  登录
	ret = Login(argc[1], argc[2]);	
	switch(ret)	
	{
		case EPWD:
			printf("login failure : Please confirm the account or password again !\n");
			return 1;
		case EMOD:
			printf("login skip failure : Maybe Account at protection mode or No logout last !\n");
			return 1;
		default :
			printf("login successfully .\n");	
	}
	
	// 发送消息
	if (SendMsg(argc[3],argc[4]) != TRUE)
		printf("massege send failure !!\n");
	else
		printf("massege send success ..\n");
	 //注销	
	ret = Logout();
	if (ret != TRUE)
	{
		printf("logout failure !\n");
	return 1;
	}
	printf("logout successfully .\n");
return 0;
}