#ifndef FETION_LIBC_H
#define	FETION_LIBC_H

#define TRUE	0
#define FALSE	1 
#define EPWD	2
#define EMOD	3
#define EMSGEMPTY	4
#define EPARSEUID	5

//#define	DEBUG	//是否打印调试信息

/*
*	fountction	:	登录
*	登录成功：TRUE 
*	登录失败：EPWD	帐号密码验证失败
*			  EMOD	账户处于保护模式中，或者最近一次登录还未注销
*/
int	Login(const char *user, const char *password);


/*
*	fountction：	注销
*	return:	FALSE/TRUE
*/
int	Logout();

/*
*	fountction：	发飞信
*	return: FALSE/TRUE
*/
int SendMsg(const char *rcvmobile, const char *msg);

#endif	// FETION_LIBC_H
